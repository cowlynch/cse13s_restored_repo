#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  // You're going to have to use argc to decide whether to open up a file, or to
  // read from stdin.
  // If you haven't seen them before, you will likely want to use the functions
  // `fgetc()` and `fputc()`.
    
    
    
    FILE *fptr = stdin;
    
    if (argc>1){                //There is at least one argument in the command line.
//        printf ("**** the argument is: %s\n\n", *(argv+1));
        if (!(fopen(*(argv+1),"r"))){
            printf("Could not open file %s\n",*(argv+1));
            exit (-1);
        }
        
        fptr=fopen(*(argv+1), "r");
    }
    char c;
    
    while((c=fgetc(fptr))!=EOF){
    if (c>='a' && c<='z'){
        c = c-('a'-'A');
        fputc(c,stdout);
    }
    else{
        fputc(c,stdout);
    }
        
    
    }
    fclose(fptr);
    return 0;

}

