#include "review.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Functions for you to implement. Most of the work for this homework will be
// done in here, but you'll also need to make changes to review.h and yelling.c.

// Problem 1
void sum_and_difference(int a, int b, int *sum, int *difference) {
  //  Your code goes here!
    *sum=a+b;
    *difference=a-b;
}

// Problem 2

// Here's the two function written recursively and iteratively.  Which one is easier to read is
// left to the eye of the beholder.  But the iteratively routine may be one or two lines longer, but
// definitely uses a lot less resources than stacking function call after function calls onto the stack
// then rewinding from there.
//

// This one ought to be recursive.
size_t ll_length(LLint *node) {
  // change this, clearly.
    if (node==NULL) return 0;
    return (ll_length(node->next)+1);       // Recursively call ll_length till you get to end of link
}

// Do this one iteratively.
size_t ll_length_iterative(LLint *node) {
    int length=0;
    while (node){
        length++;
        node=node->next;
    }
    return length;
}

// Problem 3.

void reverse_doubles_with_stack(double *array, size_t array_len) {
  // This function will reverse the order of the array pointed to by *array.
  // Make sure to define your linked list data structure in review.h, and use
  // stack behavior (LIFO) to get the numbers in the reversed order.
  // There is a way to do this without a stack data structure, I know. But this
  // is for practice with data structures.
    
    LList *node, *top;
    top=NULL;                    //  Initializes the bottom of the stack with no defined value
    for (size_t i=0;i<array_len;i++){
        node = (LList *)malloc(sizeof(LList));  //Pushes the value of the array at i onto the stack
        node->val=array[i];
        node->linkPtr=top;                      //link this node to the one under it
        top=node;                               //Make this node the top of stack
    }
    // now reverse the array by popping the stack
    while (top){
        *array=top->val;
        node=top;
        top=top->linkPtr;
        array++;
        free(node);
        node=NULL;
    }
    
    
}

// Problem 4.

tnode *word_observe(char *word, tnode *node) {
  // This function returns a pointer to a tnode because you may have to allocate
  // a new one. You might take a NULL pointer to start with.
  // Also, this means that you can very cleanly do this with recursion.
    
        
        if (node==NULL){
            // if we get to here it means we did not find a tnode with 'word' in it.  So we have
            // to allocate a new tnode and fill it in.  Note, this could return the root of the tree.
            // The caller will have to keep track of the root.
            node = (tnode *) malloc(sizeof(tnode));
            node->word=strdup(word);
            node->count=1;
            node->left=NULL;
            node->right=NULL;
            return (node);
        }
        if (strcmp(word, node->word)<0){
            node->left=word_observe(word, node->left);
        }
        else if (strcmp(word, node->word)>0){
            node->right=word_observe(word, node->right);
        }
        else{                             //  search key already exits, so update word count of node
            node->count++;
            return node;
        }
        return node;
}


int word_count(char *word, tnode *node) {
  // Default return values; here for you to change.
    if (node==NULL){
        // if we get to here it means we did not find a tnode with 'word' in it.
        return 0;
    }
    if (strcmp(word, node->word)<0){
        return word_count(word, node->left);
    }
    else if (strcmp(word, node->word)>0){
        return word_count(word, node->right);
    }
    else{                             //  search key already exits, so update word count of node
        return node->count;
    }
}


void delete_tree(tnode *node) {
  // Free the whole tree and all associated memory. This can be recursive or
  // not, your choice!
        if (node != NULL){
            delete_tree(node->left);
            delete_tree(node->right);
            free(node->word);        // free allocated memory for word string
            free(node);              // if we get to here, this has to be a end node to be taken out.
            node=NULL;
            return;
        }
    }
void bst_print(tnode *node){
    if (node != NULL){
        bst_print(node->left);
        printf("[word]: %s\n",node->word);
        bst_print(node->right);
    }    
}

