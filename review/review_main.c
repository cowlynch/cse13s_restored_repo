#include "review.h"

#include <stdio.h>
#include <stdlib.h>


int main(void) {
    
//  Problem 1 Test
    int A = 150;
    int B = 90;
    int sum, difference;
    sum_and_difference(A, B, &sum, &difference);
    printf("\n\t**********Problem 1 Output**********\n");

    printf("A=%d, B=%d\n",A,B);
    printf("The sum of %d + %d = %d\n",A,B,sum);
    printf("The difference of %d - %d = %d\n\n",A,B,difference);

//  Problem 2 Test
    LLint root;
    LLint node1, node2, node3, node4;
    root.val=0;
    root.next=&node1;
    node1.val=1;
    node1.next=&node2;
    node2.val=2;
    node2.next=&node3;
    node3.val=3;
    node3.next=&node4;
    node4.val=4;
    node4.next=NULL;
        
    size_t length_rec=ll_length(&root);
    
    printf("\n\t**********Problem 2 Output**********\n");

    printf("link list size (recursive) = %zu\n",length_rec);
       
    // Do this one iteratively.
    size_t length_iter=ll_length_iterative(&root);
    printf("link list size (iterative) = %zu\n",length_iter);
    
//  Problem 3 Test
    
    // This function will reverse the order of the array pointed to by *array.
    // Make sure to define your linked list data structure in review.h, and use
    // stack behavior (LIFO) to get the numbers in the reversed order.
    // There is a way to do this without a stack data structure, I know. But this
    // is for practice with data structures.
    
    size_t array_len=10;
    double array[10]={3.7,29.34,101.873,-1.7,1001.21, 9, 35, 45,60, 75};
    printf("\n\t**********Problem 3 Output**********\n");
    
    printf("Original Array\n");
    for (size_t i=0;i<array_len;i++){
        printf("%f, ",array[i]);
    }
    reverse_doubles_with_stack(array, array_len);
    printf("\nReversed Array\n");
    for (size_t i=0;i<array_len;i++){
        printf("%f, ",array[i]);
    }
    printf("\n");

//  Problem 4 Test
    printf("\n\t**********Problem 4 Output**********\n");
    tnode *treeRoot = NULL;
    tnode *node = NULL;
    int wrdCnt= 0;
    char *word="first";

    treeRoot = word_observe(word, treeRoot);
    word="second";
    word_observe(word,treeRoot);
    word="third";
    node=word_observe(word,treeRoot);
    word="second";
    node=word_observe(word,treeRoot);
    word="third";
    word_observe(word,treeRoot);
    word="fourth";
    word_observe(word,treeRoot);
    word="first";
    word_observe(word,treeRoot);
    
    bst_print(treeRoot);
    
    wrdCnt= word_count(word, treeRoot);
    printf ("%s word count is: %d\n",word, wrdCnt);
    
    delete_tree(treeRoot);
    
}

