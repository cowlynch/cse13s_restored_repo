#  Comments on review homework:  Winter 2020
** Due date: Wednesday, 16 March 2022

## problem 1
    The function [void sum_and_difference(int a, int b, int *sum, int *difference)] deferences the ptrs sun and difference before writing the values of sum and differences into them.

## problem 2
    For the recursive function the base case is to check for the null linked list ptr.  When reached,  you just unwind through the recursive calls by adding 1 to the return value.
    
    For the interative function, you have to keep an integer cntr that counts the number of times through the while not null ptr loop.  You then return this value when you get to the end of the linked list.

## problem 3

    My solution uses dynamically allocated memory using malloc.  So I have to release this memory before I leave the function.
    My structure for the linked list is...
    typedef struct LList {
    double val;                 //Keeps the type double value from the array in
    struct LList *linkPtr;     // ptr to the list node below the current.  If at bottom of list, ptr=NULL
} LList;

    Inside the function, I keep a ptr variable that points to the top of the stack for pushing nodes onto.
    
    Basically, I iterate over each element in the array up to the array length.  For each ellement I allocate memory for one llist structure and store the value of the array into this element.  I then push the element onto a stack.
    When all elements have been pushed onto the stack, I pop the element off the stack and place into the 1st element of the array.  I increment the array pointer (using ptr arithmatic) and pop the next element off the stack and place the value into the array.  Keep doing this till there are no more elements to pop.  After each pop, i also free the memory used by that node.
    
## problem 4
    The first function, tnode *word_observe(char *word, tnode *node);, creates the root of a BST tree if it doesn't already exist.  If the BST exists, it searches down the tree in post order search to find out where to insert the passed 'word'.  If the word doesn't exist, it adds it to the tree, setting the left and right ptrs appropriately, and setting value to zero.  If the word key already exists, it increments value by 1.
    
    The second function, int word_count(char *word, tnode *node);, searches for the word and returns the stored value of that words count.  It searches down the bst usings a post order search.
    
    The print function prints out a sorted list of words stored in the bst along with the number of times that word has occured.
    
    Finally, void delete_tree(tnode *node); deletes all nodes on the tree and free's up the memory that was allocated from word_observe.
    
## problem 5
    YELLING is pretty simple...
      1st set the FILE ptr to STDIN.
      Check to see if a file parameter was passed
      if yes, try and open the file for readwith that name.  
         If file doesn't exits, exit with a 'file doesn't exist' error.
         else set file ptr to the passed parameter (replacing stdin)
         (note, it doesn't matter if reading from stdin or from a file, as long as you set file ptr appropriately)
      use getc to get one character at a time.  If you read EOF, exit and close file (either stdin or file)
        if the read character is between a-z then subtract the difference in ascii value between ('a'-'A') and print out the upper case number.
        For all other characters, just echo them out to STDOUT.
        
    All writes are to STDOUT.
